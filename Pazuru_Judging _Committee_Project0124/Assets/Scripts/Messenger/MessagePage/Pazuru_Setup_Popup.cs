﻿using UnityEngine;
using UnityEngine.UI;

public class Pazuru_Setup_Popup :Popup
{
    [SerializeField]
    private GameObject SetUpPanel;
    
    [SerializeField]
    private Dropdown _DropDown;

    [SerializeField]
    private Sprite[] _SetupAttackType;
    private Image image;

    [SerializeField]
    private Button _StartButton;

    [SerializeField]
    private E_SpecialAttack SelectSpecialNum;

    [SerializeField]
    private int SelectNum;

    void Awake()
    {
        var obj = this.transform.GetChild(0).gameObject;
        image = obj.GetComponent<Image>();
    }

    private void Start()
    {
        _DropDown.value = 0;
    }

    public void SpecialAttack_Select1() 
    {
        var selectnow = _DropDown.value;
        switch ((E_SpecialAttack)selectnow)
        {
            case E_SpecialAttack.SP_Red:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_Red];

                if (SelectNum == 1)
                {
                    PlayerPrefs.SetInt(SaveData_Manager.KEY_ATTACK_1, (int)E_SpecialAttack.SP_Red);
                    var num = PlayerPrefs.GetInt(SaveData_Manager.KEY_ATTACK_1, 0);                    
                    Debug.Log(num+"を設定");
                }
                if (SelectNum == 2)
                {
                    PlayerPrefs.SetInt(SaveData_Manager.KEY_ATTACK_2, (int)E_SpecialAttack.SP_Red);
                    var num = PlayerPrefs.GetInt(SaveData_Manager.KEY_ATTACK_2, 0);
                    Debug.Log(num + "を設定");
                }
                break;
            case E_SpecialAttack.SP_Blue:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_Blue];

                if (SelectNum == 1)
                {
                    PlayerPrefs.SetInt(SaveData_Manager.KEY_ATTACK_1, (int)E_SpecialAttack.SP_Blue);
                    var num = PlayerPrefs.GetInt(SaveData_Manager.KEY_ATTACK_1, 0);
                    Debug.Log(num + "を設定");
                }

                if (SelectNum == 2)
                {
                    PlayerPrefs.SetInt(SaveData_Manager.KEY_ATTACK_2, (int)E_SpecialAttack.SP_Blue);
                    var num = PlayerPrefs.GetInt(SaveData_Manager.KEY_ATTACK_2, 0);
                    Debug.Log(num + "を設定");
                }
                break;
            case E_SpecialAttack.SP_Yellow:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_Yellow];
                break;
            case E_SpecialAttack.SP_Green:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_Green];
                break;
            case E_SpecialAttack.SP_HorizontalOneArray:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_HorizontalOneArray];
                break;
            case E_SpecialAttack.SP_VerticalOneArray:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_VerticalOneArray];
                break;
            case E_SpecialAttack.SP_Destroy_Cross:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_Destroy_Cross];
                break;
            case E_SpecialAttack.SP_ObliqueCross_Cross:
                image.sprite = _SetupAttackType[(int)E_SpecialAttack.SP_ObliqueCross_Cross];
                break;
        }

    }

    /// <summary>
    /// ゲームスタートボタンの開始
    /// </summary>
    public void GameStart()
    {
        // SceneManager.LoadScene("3_Reversi_GameScene");
        SetUpPanel.SetActive(true);
    }

    /// <summary>
    /// ☓ボタンを押すとポップアップが非表示になる・今後DoTweenでアニメーションを入れる予定
    /// </summary>
    public void Cancel()
    {
       // Popup_Close(_GamePlay_PopupPanel);
    }

    /// <summary>
    /// ポップアップの表示
    /// </summary>
    public void TapPlayButton()
    {
       
       // PopupStart(_GamePlay_PopupPanel);
        //_GamePlay_PopupPanel.SetActive(true);
    }



}
