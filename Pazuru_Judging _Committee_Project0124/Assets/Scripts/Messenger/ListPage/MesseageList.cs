﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MesseageList : MonoBehaviour
{
    [SerializeField]
    private GameObject _MesseageList;

    [SerializeField]
    private RectTransform _MesseagerectTransform;

    private GameObject[] _MesseageObject;     
    private string StageName = "Messeage";
    private int StageListNo = 0;
    private int ClearNum;

    [SerializeField]
    private Image ExclamationMark;

    private void Start()
    {
        //PlayerPrefs.DeleteAll();

        //クリアナンバーの更新
        ClearNum= PlayerPrefs.GetInt(SaveData_Manager.KEY_CLEAR_NUM, 0);

        //リスト表示（クリアナンバーの+1）
        StageListNo = ClearNum+1;
        _MesseageObject = new GameObject[StageListNo];

        Debug.Log(StageListNo);
        NewMessageListAdditional();

    }

    /// <summary>
    /// ClearNumに応じて、ゲームオブジェクトを生成する
    /// </summary>
    public void NewMessageListAdditional() 
    {
        for (var i = 0; i < StageListNo; i++) 
        {
            //ステージの数が全部で9個のためオブジェクトを生成しないように制限を設ける
            if (i < 9) 
            {
                _MesseageObject[i] = Instantiate(_MesseageList);
                _MesseageObject[i].transform.SetParent(_MesseagerectTransform);

                MesseageListSetting(_MesseageObject[i], i);
                Debug.Log("num" + _MesseageObject[i].GetComponent<ListObject>().GetStageNum);
                if (i == StageListNo - 1) { _MesseageObject[i].GetComponent<ListObject>().GetisRead = true; }
            }
                             
        }
    }
    public void NextScene() 
    {
        SceneManager.LoadScene("PazuruGameScene");    
    }
    private void MesseageListSetting(GameObject obj, int objectnum) 
    {
        var obj2 = obj.GetComponent<ListObject>();
        obj2.GetStageNum = objectnum;
      
    }


}
