﻿using UnityEngine;

/// <summary>
/// セーブデータ管理クラス
/// </summary>

public class SaveData_Manager : Singleton<SaveData_Manager>
{  
    //ステージ記録フラグ
    public const string KEY_STAGE_NUM = "STAGENUM";
    public const string KEY_LIST_NUM = "LISTNUM";
   
    //既読フラグ
    public const string KEY_ISREAD_NUM = "KEY_ISREAD_NUM"; 

    //クリアフラグ
    public const string KEY_CLEAR_NUM = "STAGECLEARNUM";

    //スペシャル攻撃の技の登録
    public const string KEY_ATTACK_1 = "ATTACK1";
    public const string KEY_ATTACK_2 = "ATTACK2";

    /// <summary>
    /// シングルトンの起動
    /// </summary>
    public void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

}
